const express = require('express');
const router = express.Router();
const Breed = require('../models/breed');
const { ErrorHandler } = require('../helpers/error');

// retrieve all breeds
// @route GET api/breeds
// @access Public
router.get('/breed', async (req, res, next) => {
	try {
		const breeds = await Breed.find();
		if (!breeds) {
			throw new ErrorHandler(404, `Breeds does not exists`);
		}
		res.status(200);
		res.json(breeds);
	}
	catch (error) {
		next(error)
	  }
});

// Get breed by id
// @route GET api/breed/id
// @access Public
router.get('/breed/:id', async (req, res, next) => {
	try {
		const breed = await Breed.findById(req.params.id);
		if (!breed) {
			throw new ErrorHandler(404, `Breed with id ${req.params.id} does not exists`);
		}
		res.status(200);
		res.json(breed);
	}
	catch (error) {
		next(error)
	  }
});

// search breed by name
// @route POST api/breed/name
// @access Public
router.get('/breed/search/:name', async (req, res, next) => {
	// case insensitive search
	const nameRegex = new RegExp(req.params.name, 'i');
	try {
		const getBreedByName = await Breed.find({ name: nameRegex });
		if (!getBreedByName || getBreedByName.length === 0) {
			throw new ErrorHandler(404, `Breed with name ${req.params.name} does not exists`);
		}
		res.status(200);
		res.json(getBreedByName);
	} catch (error) {
		next(error);
	}
});


// Create new breed
// @route POST api/add-breed
// @access Public
router.post('/breed', async (req, res, next) => {
	if (!req.body.name || !req.body.description) {
		throw new ErrorHandler(400, `Name and description are required`);
	}

	const breed = new Breed({
		name: req.body.name,
		description: req.body.description,
		temperament: req.body.temperament,
		origin: req.body.origin,
	});

	try {
		createBread = await breed.save();
		res.status = 201;
		res.json({ message: 'breed saved..!' });

	}
	catch (error) {
		next(error);
	}
});

module.exports = router;