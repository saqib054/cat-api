class ErrorHandler extends Error {
    constructor(statusCode, message) {
      super();
      this.statusCode = statusCode;
      this.message = message;
    }
  }

const handleError = (error, res) => {
    res.status(error.status || 500);
    res.json({ error: error.message })
};
  module.exports = {
    ErrorHandler,
    handleError
  }