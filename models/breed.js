const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BreedSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	temperament: {
		type: String
	},
	origin: {
		type: String
	}
});

BreedSchema.set('timestamps', true);
const Breed = mongoose.model('breeds', BreedSchema);
module.exports = Breed;